# rdf utils library
Providing convenient helper classes for the rdf context

## Table of Contents
1. [Build & Install](#build-install)
1. [Use](#use)
1. [License](#license)

## Build & Install
Requirements:
 * Git
 * Maven
 * Java

```bash
$ git clone https://gitlab.com/european-data-portal/rdf-utils.git
$ cd rdf-utils
$ mvn install
```

## Use
Add dependency to your project pom file:
```xml
<dependency>
    <groupId>io.piveau</groupId>
    <artifactId>rdf-utils</artifactId>
    <version>4.3.1</version>
</dependency>
```

## License

[Apache License, Version 2.0](LICENSE.md)
