package io.piveau.utils;

import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFParser;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.vocabulary.DCAT;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.ORG;
import org.apache.jena.vocabulary.SKOS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.Normalizer;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
/**
 * Created by sim on 10.02.2017.
 */
public class JenaUtils {
    private static final Logger log = LoggerFactory.getLogger(JenaUtils.class);

    private static ModelExtract extractor = new ModelExtract(new StatementBoundaryBase() {
        @Override
        public boolean stopAt(Statement s) {
            return s.getPredicate().equals(FOAF.primaryTopic);
        }
    });

    public static final String NS_HYDRA = "http://www.w3.org/ns/hydra/core#";

    public static final Map<String, String> DCATAP_PREFIXES = Collections.unmodifiableMap(
            new HashMap<String, String>() {{
                put("dcat", DCAT.NS);
                put("skos", SKOS.uri);
                put("foaf", FOAF.NS);
                put("dct", DCTerms.NS);
                put("gmd", "http://www.isotc211.org/2005/gmd#");
                put("v", "http://www.w3.org/2006/vcard/ns#");
                put("adms", ADMS.NS);
                put("spdx", "http://spdx.org/rdf/terms#");
                put("schema", "http://schema.org/");
                put("locn", "http://www.w3.org/ns/locn#");
                put("org", ORG.NS);
                put("time", "http://www.w3.org/2006/time#");
                put("hydra", NS_HYDRA);
            }});

    public static Model extractResource(Resource resource) {
        try {
            Model d = extractor.extract(resource, resource.getModel());
            d.setNsPrefixes(JenaUtils.DCATAP_PREFIXES);
            return d;
        } catch (Exception e) {
            log.error("model extraction", e);
            return null;
        }
    }

    public static Model read(byte[] content, Lang lang) {
        Model model = ModelFactory.createDefaultModel();
        RDFParser.create()
                .source(new ByteArrayInputStream(content))
                .lang(lang)
                .base("")
                .checking(false)
                .parse(model);
//        RDFDataMgr.read(model, new ByteArrayInputStream(content), "", lang);
        return model;
    }

    public static Model read(byte[] content, String contentType) {
        return read(content, mimeTypeToLang(contentType));
    }

    public static String write(Model model, Lang lang) {
        StringWriter writer = new StringWriter();
        RDFDataMgr.write(writer, model, lang);
        return writer.toString();
    }

    public static String write(Model model, String contentType) {
        return write(model, mimeTypeToLang(contentType));
    }

    public static String findIdentifier(Resource resource) {
        StmtIterator it = resource.listProperties(DCTerms.identifier);
        if (it.hasNext()) {
            RDFNode obj = it.next().getObject();
            if (obj.isLiteral()) {
                return obj.asLiteral().getString();
            } else {
                // cause it is standard we force a warning here
                // but we could do different and handle resource as well...
//                return obj.asResource().getURI();
                return obj.asLiteral().getString();
            }
        } else {
            String uri = resource.getURI();
            int idx = uri.lastIndexOf("/");
            if (idx != -1) {
                return uri.substring(idx + 1);
            } else {
                return uri;
            }
        }
    }

    public static Lang mimeTypeToLang(String dataMimeType) {
        Lang lang = Lang.NTRIPLES;
        if (dataMimeType != null) {
            int idx = dataMimeType.indexOf(";");
            String type = idx != -1 ? dataMimeType.substring(0, idx) : dataMimeType;
            switch (type.trim()) {
                case "application/rdf+xml":
                    lang = Lang.RDFXML;
                    break;
                case "application/ld+json":
                case "application/json":
                    lang = Lang.JSONLD;
                    break;
                case "text/turtle":
                    lang = Lang.TURTLE;
                    break;
                case "text/n3":
                    lang = Lang.N3;
                    break;
                case "application/trig":
                    lang = Lang.TRIG;
                    break;
                case "application/n-triples":
                    lang = Lang.NTRIPLES;
                    break;
                default:
            }
        }
        return lang;
    }

    public static String normalize(String id) {
        try {
            id = URLDecoder.decode(id, "UTF-8");
        } catch (UnsupportedEncodingException e) {

        }
        String normalized  = Normalizer.normalize(id, Normalizer.Form.NFKD);
        //remove all '%'
        //replace non-word-characters with '-'
        //then combine multiple '-' into one
        return normalized.replaceAll("%", "").replaceAll("\\W", "-").replaceAll("-+", "-").toLowerCase();
    }

}
