package io.piveau.utils;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.codec.digest.MessageDigestAlgorithms;

public class Hash {

    public static String asHexString(String data) {
        return new DigestUtils(MessageDigestAlgorithms.MD5).digestAsHex(data);
    }

    public static byte[] asByteArray(String data) {
        return new DigestUtils(MessageDigestAlgorithms.MD5).digest(data);
    }

}
