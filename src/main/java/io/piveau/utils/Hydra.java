package io.piveau.utils;

import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.RDF;

import java.net.MalformedURLException;
import java.net.URL;

public class Hydra {

    private static final Model m = ModelFactory.createDefaultModel();

    private static final Resource PAGED_COLLECTION;
    private static final Resource PARTIAL_COLLECTION_VIEW;

    private static final Property TOTAL_ITEMS;

    private static final Property NEXT;
    private static final Property NEXT_PAGE;

    static {
        PARTIAL_COLLECTION_VIEW = m.createResource(JenaUtils.NS_HYDRA + "PartialCollectionView");
        PAGED_COLLECTION = m.createResource(JenaUtils.NS_HYDRA + "PagedCollection");

        TOTAL_ITEMS = m.createProperty(JenaUtils.NS_HYDRA + "totalItems");
        NEXT = m.createProperty(JenaUtils.NS_HYDRA + "next");
        NEXT_PAGE = m.createProperty(JenaUtils.NS_HYDRA + "nextPage");
    }

    private Resource paging;
    private Property next;

    private int total;

    private String base;

    private Hydra(Resource paging, Property next, int total, String base) {
        this.paging = paging;
        this.next = next;
        this.total = total;
        this.base = base;
    }

    public static Hydra findPaging(Model model, String base) {
        ResIterator it = model.listResourcesWithProperty(RDF.type, PAGED_COLLECTION);
        Property next = NEXT_PAGE;
        int total = 0;
        Resource hydra = null;
        if (!it.hasNext()) {
            next = NEXT;
            it = model.listResourcesWithProperty(RDF.type, PARTIAL_COLLECTION_VIEW);
            if (it.hasNext()) {
                total = model.listObjectsOfProperty(TOTAL_ITEMS).next().asLiteral().getInt();
                hydra = it.nextResource();
            }
        } else {
            hydra = it.nextResource();
            total = hydra.getProperty(TOTAL_ITEMS).getInt();
        }
        return new Hydra(hydra, next, total, base);
    }

    public int total() {
        return total;
    }

    public String next() {
        if (paging == null) {
            return null;
        }

        if (next == NEXT) {
            return paging.hasProperty(next) ? handleBrokenHydra(paging.getProperty(next).getResource().getURI()) : null;
        } else {
            return paging.hasProperty(next) ? handleBrokenHydra(paging.getProperty(next).getString()) : null;
        }
    }

    private String handleBrokenHydra(String next) {
        if (base == null) {
            return next;
        } else {
            try {
                URL baseUrl= new URL(base);
                URL nextUrl = new URL(next);
                // deal here with base
                return baseUrl.getProtocol() + "://"+ baseUrl.getAuthority() + baseUrl.getPath() + "?" + nextUrl.getQuery();
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return next;
            }
        }
    }

}
